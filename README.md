#Synopsis

Create Datastructure for Jamman Looper

##info file
purposes: 
- cut a wave file using sox, 
- adjust the bpm
- adjust beat count
- set loop or oneshot parameter, 
- set fadeout mode
bpm=120
beats=4
cutfrom=2 # 2 measures countin
cutcount=16 # cut 16 measures
stopmode=fade #fade stop end
loopmode=loop #oneshot

JamManStereo/Patch01/patch.xml
- RhythmType
- StopMode
- SettingsVersion
- ID
- OriginID

JamManStereo/Patch01/PhraseA/Phrase.xml
- BeatsPerMeasure
- BpmValidated
- IsLoop
- IsReversed
- SettingsVersion
- AudioVersion
- ID
- OriginID

