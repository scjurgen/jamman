#!/usr/bin/env python3


import os
import re
import sys

def create_default_info(infofile):
    default_values = """
#cutfrom = 2
#cutcount = 16
beats = 4
bpm   = 120
reverse = false
stopmode = fade
#stopmode = stop
#stopmode = until end
loopmode = loop
#loopmode = one shot
"""
    with open(infofile, "w") as text_file:
        text_file.write(default_values)
    print("*** Warning: written default info in {0}".format(infofile))



def create_patch(wavefile, infofile, index):
    bpm = 120
    cut = False
    cutfrom = 2
    cutcount = 12
    beats = 4
    is_loop = False
    is_reversed = False
    stop_mode = "fade"
    if infofile is not None:
        f = open(infofile, 'r')
        lines = [line.rstrip('\n') for line in f]
        for l in lines:
            values = re.split('\s*[=:]\s*', l.strip())
            print(values)
            try:

                if len(values) == 2:
                    key = values[0]
                    if key in ['bpm', 'BPM', 'BeatsPerMinute']:
                        bpm = int(values[1])
                    elif key in ['beats', 'beats count']:
                        beats = int(values[1])
                    elif key in ['cutfrom', 'cut from', 'cut here']:
                        cutfrom = int(values[1])
                        cut = True
                    elif key in ['cutcount', 'cut count', 'cut until']:
                        cutcount = int(values[1])
                        cut = True
                    elif key in ['loopmode', 'loop']:
                        if values[1] in ['0', 'one', 'shot', 'one shot']:
                            is_loop = False
                        else:
                            is_loop = True
                    elif key in ['stop', 'stopmode']:
                        if values[1] in ['fade', 'fadeout', 'fade out']:
                            stop_mode = "fade"
                        elif values[1] in ['stop']:
                            stop_mode = "stop"
                        elif values[1] in ['until end']:
                            stop_mode = "until end"
                        else:
                            print("Error: unknown stopmode {0}".format(values[1]))
                    elif key in ['reverse', 'rev', 'backwards']:
                        if values[1] in ['1', 'True', 'true', 'TRUE']:
                            is_reversed = True
                        elif values[1] in ['0', 'False', 'false', 'FALSE']:
                            is_reversed = True
                        else:
                            print("Error: unknown reverse {0} (boolean expected)".format(values[1]))
            except:
                pass

    print("BPM={0}".format(bpm))
    print("Beats={0}".format(beats))
    if is_loop:
        print("is a loop")
    else:
        print("is a one shot")
    if is_reversed:
        print("is a reversed")
    else:
        print("is NOT reversed")
    if cut:
        print("cutfrom={0} for {1}".format(cutfrom, cutcount))
    print("stop_mode={0}".format(stop_mode))


list_of_files = [] 
try:
    start_path = sys.argv[1]
    targetpath = sys.argv[2]
    first_index = int(sys.argv[3])
except:
    print("Usage {} <wavepath> <targetpath> <firstIndex>".format(sys.argv[0]))
    sys.exit(0)

for path, dirs, files in os.walk(start_path):
    for filename in files:
        if filename.endswith(".wav"):
            list_of_files.append(os.path.join(path, filename))
list_of_files.sort()

index = first_index
for p in list_of_files:
    pre, ext = os.path.splitext(p)
    infofile = pre+".txt"
    print("pre:"+infofile)
    if os.path.isfile(infofile):
        print("has info")
    else:
        create_default_info(infofile)
        infofile = None
    print("#{index:02d} {path}".format(index=index, path=p))
    create_patch(p, infofile, index)
    index += 1

